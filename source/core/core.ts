export interface IRecipient {
    id: string;
    username: string;
    email: string;
}

export interface ISendGridRequest {
    kind: string,
    verificationToken?: string
}

export interface ISendGridResponse {
    fail?: boolean
}

export interface IMessage extends Record<string, any> {
    to: string
    from: string
    subject: string
    text: string
}

export interface IUserRegistrationNotificationRequest {
    kind: "register"
    recipient: IRecipient
}

export interface IUserForgotPasswordNotificationRequest {
    kind: "forgot-password"
    recipient: IRecipient
    forgotPasswordToken: string
}

export interface IUserResetPasswordNotificationRequest {
    kind: "reset-password"
    recipient: IRecipient
    resetPasswordToken: string
}

export type INotificationRequest = IUserRegistrationNotificationRequest | IUserForgotPasswordNotificationRequest | IUserResetPasswordNotificationRequest


