import { INotificationRequest, IMessage } from "./core";

export interface IApiKey {
    key: string
}

export interface IApiKeys {
    sendgrid: IApiKey
}

export interface ISendGridService {
    createMessage(notificationRequest: INotificationRequest): IMessage
    sendEmail(message: IMessage): Promise<void>
}

