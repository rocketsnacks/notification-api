import { INotificationRequest, IMessage } from "../core"
import * as Logger from "bunyan"
import { DbConnection } from '../../data/dbConnection'
import { ISendGridService, IApiKeys } from '../services'
import * as sgMail from '@sendgrid/mail'

export class SendGridService implements ISendGridService {
    constructor(
        private apiKeys: IApiKeys,
        private dbConnection: DbConnection,
        private logger: Logger
    ) {}

    assertNever(arg: never) {
        throw new Error('Should never hit this')
    }       

    createMessage(notifyRequest: INotificationRequest): IMessage {
        let msg
        const kind = notifyRequest.kind;
        switch(kind) {
            case "register": {
                return msg = {
                    to: notifyRequest.recipient.email,
                    from: 'audrey@rocketmakers.com',
                    subject: `Thanks for registering ${notifyRequest.recipient.username}`,
                    text: 'howdy'
                }
            }
            case "forgot-password": {
                return msg = {
                    to: notifyRequest.recipient.email,
                    from: 'audrey@rocketmakers.com',
                    subject: 'forgot-password',
                    text: 'howdy'
                }
            }
            case 'reset-password': {
                return msg = {
                    to: notifyRequest.recipient.email,
                    from: 'audrey@rocketmakers.com',
                    subject: 'reset-password',
                    text: 'howdy'
                } 
            }
            default: {
                this.assertNever(kind)
                throw new Error('')
            }
        }
    }

    async sendEmail(msg: IMessage) {
        sgMail.setApiKey(this.apiKeys.sendgrid.key)
        sgMail.send(msg)
    }
}


