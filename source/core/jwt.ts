export interface IJwtUser {
    id: string;
    username: string;
    roles: string[]
  }
  