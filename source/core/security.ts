export interface ITokenGenerator {
    generateToken(): Promise<string>;
  }
  
  export interface ISaltGenerator {
    generateSalt(): Promise<string>;
  }
  
  export interface IHasher {
    hash(input: string): Promise<string>;
    compareHash(input: string, actualHash: string): Promise<boolean>;
  }
  