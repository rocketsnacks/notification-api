import { api } from 'rokot-apicontroller'
import { 
    ISendGridResponse,
    INotificationRequest
} from '../core/core'
import {
    IRequest
} from '../server/expressRequest'
import { InjectTypes } from '../ioc/injectTypes'
import { ISendGridService } from '../core/services';
import express = require('express');
import { UserType } from '../server/tokenParser';
import { DbConnection } from '../data/dbConnection';

class Middleware {
    @api.middlewareProviderFunction("protect", 1)
    static protect = (userType: UserType) => {
      return (req: express.Request, res: express.Response, next: express.NextFunction) => {
        if (req.userType !== userType) {
          res.status(401).send("Unauthorized");
          return
        }
  
        next()
      };
    }
  
    @api.middlewareFunction("authenticated")
    static authenticated = (req: express.Request, res: express.Response, next: express.NextFunction) => {
      if (!req.userType) {
        res.status(401).send("Unauthorized");
        return
      }
  
      next()
    };
  
    @api.middlewareFunction("user")
    static user = Middleware.protect("user")
  
    @api.middlewareFunction("system")
    static system = Middleware.protect("system")
  
}

@api.controller(InjectTypes.NotificationController, "notify")
export class NotificationController {
    constructor(
        private sendGridService: ISendGridService
    ) { }

    /* Send a notificaion via SendGrid API */
    @api.route("sendgrid") 
    @api.verbs("post")
    async sendGrid(req: IRequest<INotificationRequest, ISendGridResponse, void, void>) {
        const msg = this.sendGridService.createMessage(req.body)
        this.sendGridService.sendEmail(msg)
        req.sendNoContent()
    }
}