import { IServiceConfig, run, parseEnv } from './ioc/bootstrap'
import { ConsoleLogger } from 'rokot-log';

const logger = ConsoleLogger.create("@rocketmakers/notify-module", { level: "trace", mode: "dev" })

const SG_API_KEY = process.env.SENDGRID_API_KEY

const config: IServiceConfig = {
    apiKeys: {
        sendgrid: {
            key: SG_API_KEY || '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'
        }
    },
    api: {
        port: parseEnv(process.env.API_PORT || "3000") ,
        userAuthSecret: "auth-secret-:)", 
        systemAuthSecret: "system-auth-secret-:)"
    }
}

async function boot() {
    const ok = await run(config, logger)
    if (!ok) {
        logger.error("Exiting process")
        process.exit(1)
    }
}

boot()
