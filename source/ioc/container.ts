import { Container, injectable, decorate, inject } from 'inversify'
export const container = new Container()

//this function decorates each class registered in the container with the injectable() decorator (required for IOC to create class instances) 
//and inject() on constructor params for those classes
//then it binds the key to the class type in the container
export function register(classType: any, key: string, ...ctorParams: string[]) {
    decorate(injectable(), classType);

    //this will inject constructor params when a class instance is created
    ctorParams.forEach((c, i) => {
      decorate(inject(c), classType, i);
    })
  
    return container.bind(key).to(classType)
  }
  