export class InjectTypes {
    static NotificationController = "NotificationController"
    static Api = "Api"
    static ApiConfig = "ApiConfig"
    static ApiKeys = "ApiKeys"
    static SendGridService = "SendGridService"
    static Logger = "Logger"    
    static DbConnection = "DbConnection"
    // static Logger = Symbol("Logger")
    static ConnectionOptions = "ConnectionOptions"
}