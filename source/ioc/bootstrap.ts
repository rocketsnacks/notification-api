import { register, container } from './container'
import { NotificationController } from '../controllers/notificationController';
import { InjectTypes } from './injectTypes'
import { IApiConfig, Api } from "../server/api"
import { IApiKeys } from '../core/services';
import { Logger } from 'rokot-log';
import { SendGridService } from '../core/services/sendGridService';
import { ConnectionOptions } from 'typeorm';
import { DbConnection } from '../data/dbConnection';

register(Api, InjectTypes.Api,
    InjectTypes.Logger,
    InjectTypes.ApiConfig
)

register(DbConnection, InjectTypes.DbConnection,
    InjectTypes.Logger).inSingletonScope()    


register(NotificationController, InjectTypes.NotificationController,
    InjectTypes.SendGridService
)

export interface IServiceConfig {
    apiKeys: IApiKeys,
    api: IApiConfig
}

export function parseEnv(env:string) {
    return Number.parseInt(env, 10)
}

export async function run(config: IServiceConfig, logger: Logger) {

    logger.info("Binding")
    logger.info("-- Core Services")
    container.bind<Logger>(InjectTypes.Logger).toConstantValue(logger)

    logger.info("-- Configuration")
    container.bind<IApiConfig>(InjectTypes.ApiConfig).toConstantValue(config.api)
    container.bind<IApiKeys>(InjectTypes.ApiKeys).toConstantValue(config.apiKeys)

    logger.info("-- Notification Services")

    register(SendGridService, InjectTypes.SendGridService,
        InjectTypes.ApiKeys,
        InjectTypes.DbConnection,
        InjectTypes.Logger
    )

    logger.info("Testing Connection")
    const connection = container.get<DbConnection>(InjectTypes.DbConnection);
    const ok = await connection.test()
    if (!ok) {
      logger.info("-- Failed")
      connection.destroy()
      return false
    }
    logger.info("-- Passed")
  
    
    logger.info("Starting Api")
    const api = container.get<Api>(InjectTypes.Api)
    if (!api.run()) {
        logger.info("-- Failed")
        return false
    }
    logger.info("-- Started")
    return true
}


