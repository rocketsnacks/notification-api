//import * as cookieParser from 'cookie-parser'
import * as express from 'express'
import * as jwt from 'jsonwebtoken'
import { Logger } from 'rokot-log'
import { IJwtUser } from '../core/jwt';

declare global {
  namespace Express {
    export interface Request {
      identity?: IJwtUser
      userType?: UserType
    }
  }
}
export type UserType = "user" | "system"

interface JwtToken<T> {
  data: T
  iat: string
  exp?: string
}

export function generateToken(data: IJwtUser, authSecret: string) {
  return jwt.sign({ data }, authSecret)
}

export function parseToken(
  logger: Logger,
  authSecret: string,
  userType: UserType
): express.RequestHandler {
  return (req, res, next) => {
    if (req.userType) {
      next()
      return
    }
    const token = getAuthToken(userType, req, logger)
    if (!token) {
      next()
      return
    }

    // Setting the clockTimestamp via a service allows support for automated testing
    const jwtOptions: jwt.VerifyOptions = {
      //clockTimestamp: timeService.secondsTimestamp()
    }

    jwt.verify(token, authSecret, jwtOptions, async (err, userToken: JwtToken<IJwtUser>) => {
      if (err) {
        logger.warn(`JWT verify error: ${err.message}`)
        return next()
      }
      req.userType = userType
      req.identity = userToken.data

      next()
    })
  }
}

function getAuthToken(userType: UserType, req: express.Request, logger: Logger) {
  // const cookie: string = req.cookies[authCookieName]
  // if (cookie) {
  //   return cookie
  // }
  const authHeader = req.header(userType === "system" ? 'SystemAuthorization' : 'Authorization')
  if (!authHeader) {
    return
  }

  const match = authHeader.match(/^Bearer (.+)$/)
  if (!match) {
    return
  }

  return match[1]
}
