import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import { Logger } from 'rokot-log'
import { createDefaultRuntimeApi  } from 'rokot-apicontroller'
import { CustomExpressRouteBuilder } from './expressRequest'
import { container } from '../ioc/container';
import { parseToken } from './tokenParser';

export interface IApiConfig {
    port: number,
    errorRequestHandler?: express.ErrorRequestHandler,
    userAuthSecret: string,
    systemAuthSecret: string
}

export class Api {
    constructor(
        private logger: Logger,
        private config: IApiConfig
    ) {}

    run() {
        const errorRequestHandler: express.ErrorRequestHandler = (err, req, res, next) => {
            this.logger.error(err)
            res.status(500).send(err.message || err)
        }

        const app = express()

        app.set("etag", false)
        app.use(cors({ credentials: true, origin: true }))
        app.disable('x-powered-by')
        app.use(bodyParser.json())
        app.use(bodyParser.urlencoded({ extended: true }))


        app.use(parseToken(this.logger, this.config.userAuthSecret, "user"))
        app.use(parseToken(this.logger, this.config.systemAuthSecret, "system"))


        const runtimeApi = createDefaultRuntimeApi(this.logger)
        if (runtimeApi) {
            this.logger.info(runtimeApi.controllers ? (runtimeApi.controllers.map(c => c.name).join(", ") || "[EMPTY]") : "NULL")
        }

        const builder = new CustomExpressRouteBuilder(this.logger, app, (cc, n) => container.get<any>(n))
        const ok = builder.build(runtimeApi)
        if (!ok) {
            this.logger.error("Unable to build routes")
            return false
        }
        

        app.use(this.config.errorRequestHandler || errorRequestHandler)
        app.listen(this.config.port, () => {
            this.logger.info(`server is up and running on port ${this.config.port}`)
        })

        return true
    }

}