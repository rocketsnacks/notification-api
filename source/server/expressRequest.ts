import { ExpressApiRequest, ExpressRouteBuilder, IExpressRequest, IExpressApiRequest } from "rokot-apicontroller";

//this needs to be generic type so you can eventually pass in different types of requests dependent on route
export interface IRequest<TBody, TResponse, TParams, TQuery> extends IExpressApiRequest<TBody, TResponse, TParams, TQuery> {
    email?: string
}

export class CustomExpressApiRequest<TBody, TResponse, TParams, TQuery> extends ExpressApiRequest<TBody, TResponse, TParams, TQuery> implements IRequest<TBody, TResponse, TParams, TQuery>{
    email?: string
}

export class CustomExpressRouteBuilder extends ExpressRouteBuilder {
    protected createHandler(req: IExpressRequest) {
      return new CustomExpressApiRequest<any, any, any, any>(req)
    }
}
  