import { Logger } from "rokot-log";
import { createConnection, Connection } from "typeorm";

export class DbConnection {
  public connection: Connection
  constructor(private logger: Logger) {
  }

  async test(): Promise<boolean> {
    try {
      this.connection = await createConnection()
      await this.connection.query('select 1 as result')
      return true
    } catch (error) {
      this.logger.error(error, "DB connection failed")
      return false
    }
  }

  async destroy(): Promise<void> {
    try {
      if (this.connection) {
        return await this.connection.close()
      }
    }
    catch (e) {
      this.logger.error(e, `Error while destroying knex connection`)
    }
  }
}
